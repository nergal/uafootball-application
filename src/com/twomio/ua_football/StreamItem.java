package com.twomio.ua_football;

import java.io.Serializable;

public class StreamItem implements Serializable {
	private static final long serialVersionUID = 2310698779687082782L;
	
	public String id_str;
	public String message;
	
	public StreamItem(String id_str, String message) {
		super();
		
		this.id_str = id_str;
		this.message = message;
	}
	
	public String toString() {
		return message;
	}
}
