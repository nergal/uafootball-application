package com.twomio.ua_football;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * sections. We use a {@link android.support.v4.app.FragmentPagerAdapter} derivative, which will
     * keep every loaded fragment in memory. If this becomes too memory intensive, it may be best
     * to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding tab.
        // We can also use ActionBar.Tab#select() to do this if we have a reference to the
        // Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            
            switch (i) {
            case 0:
            	fragment = new FragmentStream();
            	break;
            case 1:
            	fragment = new FragmentPhoto();
            	break;
            default:
            	fragment = new FragmentText();
            }
            
            // Bundle args = new Bundle();
            // args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
            // fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: return getString(R.string.title_stream).toUpperCase();
                case 1: return getString(R.string.title_photo).toUpperCase();
                case 2: return getString(R.string.title_text).toUpperCase();
            }
            return null;
        }
    }

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     */
    public static class FragmentPhoto extends Fragment {
        public FragmentPhoto() {
        }
        
        private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 666;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
        	View v = inflater.inflate(R.layout.fragment_photo, null);
        	Button btn = (Button) v.findViewById(R.id.takeShot);
        	btn.setOnClickListener(new Button.OnClickListener() {
        		@Override
                public void onClick(View v) {
        			Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(imageIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }        		
        	});
        	return v;
        }
        
        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
        	super.onActivityResult(requestCode, resultCode, data);
        	
        	if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
        		if (resultCode == RESULT_OK) {
        			ImageView img = (ImageView) getView().findViewById(R.id.imageView1);

					Bundle extras = data.getExtras();
					Bitmap currentImage = (Bitmap) extras.get("data");
					img.setImageBitmap(currentImage);
        		} else if (resultCode == RESULT_CANCELED) {
        			// Пользователь отменил съемку
        		} else {
        			// Произошла ошибка
        		}
        	}
        }
    }
    
    public static class FragmentText extends Fragment {
        public FragmentText() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            TextView textView = new TextView(getActivity());
            textView.setGravity(Gravity.CENTER);
            textView.setText("Text item");
            return textView;
        }
    }
    
    public static class FragmentStream extends Fragment {
    	static ListView listView;
    	
    	public class ChunkedResponseHandler implements ResponseHandler<String> {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                HttpEntity entity = response.getEntity();
                InputStream in = entity.getContent();
                StringBuffer out = new StringBuffer();
                byte[] b = new byte[4096];
                int n =  in.read(b);
                while (n > 0) {
                    out.append(new String(b, 0, n));
                    n = in.read(b);
                }
                
                return out.toString();
            }
        }
    	
    	public class StreamLoadingTask extends AsyncTask<Void, StreamItem, Void> {
			@SuppressWarnings("unchecked")
			@Override
			protected void onPreExecute() {
				Log.v("parser", "Clear list");
				((ArrayAdapter<StreamItem>) listView.getAdapter()).clear();
			}
			 
			@Override
			protected Void doInBackground(Void... arg0) {
	    		try {
	    			String url = getString(R.string.stream_fill_uri);
	    			Log.i("parser", "Fetching data from " + url);
	    			
		    		HttpClient client = new DefaultHttpClient();
		    		HttpGet httpGet = new HttpGet(url);
		    		
		    		ResponseHandler<String> responseHandler = new ChunkedResponseHandler();
	    			JSONObject response = new JSONObject(client.execute(httpGet, responseHandler));
	    			JSONArray data = (JSONArray) response.get("data");
	    			
	    			for (int i = 0; i < data.length(); i++) {
	    				JSONObject jo = (JSONObject) data.getJSONObject(i);
	    				publishProgress(new StreamItem(jo.getString("id"), jo.getString("name")));
	    			}
	    		} catch (ClientProtocolException e) {
	    			e.printStackTrace();
	    		} catch (JSONException e) {
	    			e.printStackTrace();
	    	    } catch (IOException e) {
	    	    	e.printStackTrace();
	    	    } finally {
	    	    	Log.i("parser", "Fetching success");
	    	    }
	    		
	    	    return null;
			}
			
			@SuppressWarnings("unchecked")
			protected void onProgressUpdate(StreamItem... items) {
				((ArrayAdapter<StreamItem>) listView.getAdapter()).add(items[0]);
			}
    	}

        public FragmentStream() {
        }
        
        @Override
        public void onCreate(Bundle savedInstanceState) {
        	super.onCreate(savedInstanceState);
        	
//			view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//        		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
//        			Intent i = new Intent(getActivity().getApplicationContext(), FragmentStream.class);
//        			Bundle bundle = new Bundle();
//        			Log.i("parser", position+"");
//        			bundle.putSerializable("param1", ((ArrayAdapter<StreamItem>) getListAdapter()).getItem(position));
//        			i.putExtras(bundle);
//        			startActivity(i);
//        		}
//        	});
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        	listView = (ListView) inflater.inflate(R.layout.fragment_stream, null);
        	
        	ArrayList<StreamItem> listItems = new ArrayList<StreamItem>();
        	ArrayAdapter<StreamItem> ar = new ArrayAdapter<StreamItem>(listView.getContext(), R.layout.list_item, listItems);
        	
        	listView.setAdapter(ar);
        	
        	new StreamLoadingTask().execute();
        	
            return listView;
        }
    }
}
